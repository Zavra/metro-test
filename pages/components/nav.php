<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
    <a class="navbar-brand" href="/">
        <img src="/assets/img/logo.svg">
    </a>

     <?php if (!empty($GLOBALS['user'])) { ?>

         <div class="nav-user">
             <div class="user__welcome">Welcome, <?= $GLOBALS['user']->getName() ?>!</div>
             <a class="user__logout" href="/logout"><i class="metro-logout"></i></a>
         </div>

     <?php   } else {
            echo '<div class="nav-user"><a href="/login">Login</a></div>';
        }
     ?>

</nav>