<!DOCTYPE html>
<html lang="en">

<?php include_once ('components/head.php')?>

<body>

<?php include_once ('components/nav.php')?>

<div class="container site-body ">
    <div class="row">
        <div class="col-12">

            <?php if (isset($_SESSION['msg_error'])) { ?>
                <div class="alert alert-danger mt-5" role="alert">
                    <?= $_SESSION['msg_error'] ?>
                </div>
            <?php } ?>

            <?php if (isset($_SESSION['msg_success'])) { ?>
                <div class="alert alert-success mt-5" role="alert">
                    <?= $_SESSION['msg_success'] ?>
                </div>
            <?php } ?>

            <div class="d-flex justify-content-between align-items-center mt-5">
                <h1 class="">Guestbook</h1>
                <a href="/post" class="btn btn-success">Add new post</a>
            </div>

            <?php if(sizeof($posts)) { ?>
                <?php foreach($posts as $post) { ?>
                <div class="post__item">
                    <h3><?= $post->title ?></h3>
                    <div class="item__details">
                        <div class="details__item">
                            <i class="metro-calendar"></i>
                            <div class=""><?= date('j M, Y', $post->date) ?></div>
                        </div>
                        <div class="details__item">
                            <i class="metro-user"></i>
                            <div class=""><?= $post->user->getName() ?></div>
                        </div>

                        <?php if ($post->status == 'pending') { ?>
                            <div class="details__item pending">
                                <i class="metro-exclamation"></i>
                                <div class="">Pending</div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="item__body">
                        <?php if ($post->image) {?>
                            <div class="body__img" style="background-image: url('<?= $post->image ?>')"></div>
                        <?php } else {
                            echo nl2br($post->text);
                        } ?>
                    </div>

                    <?php if (!empty($GLOBALS['user']) && $GLOBALS['user']->isAdmin()) { ?>

                        <div class="item__actions">

                            <a href="/post/<?= $post->id ?>" class="action btn btn-outline-primary btn-sm">
                                <i class="metro-edit"></i> Edit
                            </a>

                            <?php if ($post->status == 'pending') { ?>
                                <form method="post" action="/approve" class="action">
                                    <input type="hidden" name="post_id" value="<?= $post->id ?>">
                                    <button type="submit" class="btn btn-outline-success btn-sm">
                                        Approve
                                    </button>
                                </form>
                            <?php } ?>

                            <form method="post" action="/delete" class="action">
                                <input type="hidden" name="post_id" value="<?= $post->id ?>">
                                <button type="submit" class="btn btn-outline-danger btn-sm">
                                    <i class="metro-trash"></i> Delete
                                </button>
                            </form>
                        </div>

                    <?php } ?>
                </div>
            <?php } ?>
            <?php } else { ?>
                <div class="alert alert-secondary mt-5 text-center" role="alert">
                    No posts yet
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php include_once ('components/foooter.php')?>

</body>


</html>
