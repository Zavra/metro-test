<!DOCTYPE html>
<html lang="en">

<?php include_once ('components/head.php')?>

<body>

<?php include_once ('components/nav.php')?>

<div class="site-body login-container">
    <div class="login-form">
        <h4 class="text-center mb-4">Login</h4>

        <?php if (isset($_SESSION['form_error'])) { ?>
            <div class="alert alert-danger" role="alert">
                <?= $_SESSION['form_error'] ?>
            </div>
        <?php } ?>

        <form method="POST">
            <div class="form-group">
                <label>Email address</label>
                <input name="email" type="email" class="form-control" placeholder="Enter email"
                       value="<?php if (isset($_SESSION['old_email'])) { echo $_SESSION['old_email']; } ?>"
                >
            </div>
            <div class="form-group">
                <label>Password</label>
                <input name="password" type="password" class="form-control" placeholder="Password"
                       value="<?php if (isset($_SESSION['old_password'])) { echo $_SESSION['old_password']; } ?>">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success">Login</button>
            </div>
        </form>
    </div>
</div>

<?php include_once ('components/foooter.php')?>

</body>
</html>
