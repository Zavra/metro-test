<!DOCTYPE html>
<html lang="en">

<?php include_once ('components/head.php')?>

<body>

<?php include_once ('components/nav.php')?>

<div class="container site-body">
    <div class="row">
        <div class="col-12">

            <?php if (isset($_SESSION['msg_error'])) { ?>
                <div class="alert alert-danger mt-5" role="alert">
                    <?= $_SESSION['msg_error'] ?>
                </div>
            <?php } ?>

            <h1 class="mt-5 mb-5">
                <?= (isset($_SESSION['old_id'])) ? 'Edit Post' : 'Add Post' ?>
            </h1>

            <form method="POST" enctype="multipart/form-data">
                <?php if(isset($_SESSION['old_id'])) { ?>
                    <input type="hidden" value="<?= $_SESSION['old_id'] ?>" name="id">
                <?php } ?>
                <div class="form-group">
                    <label>Title</label>
                    <input name="title" required type="text" class="form-control" placeholder="Enter post title"
                           value="<?php if (isset($_SESSION['old_title'])) { echo $_SESSION['old_title']; } ?>"
                    >
                </div>

                <div class="form-group">
                    <div class="alert alert-warning" role="alert">
                        What do you want to add, <span class="js-post-content post-content" data-content="text">text</span>
                        or
                        <span class="js-post-content post-content" data-content="image">image</span>?
                    </div>
                </div>

                <div class="form-group post-content-control" id="post-text"
                <?php if (isset($_SESSION['old_text'])) { echo 'style="display: block;"'; }?>>
                    <label>Text</label>
                    <textarea name="text" class="form-control" placeholder="Add post text here...."><?php if (isset($_SESSION['old_text'])) { echo $_SESSION['old_text']; } ?></textarea>
                </div>

                <div class="form-group post-content-control" id="post-image"
                    <?php if (isset($_SESSION['old_image'])) { echo 'style="display: block;"'; }?>>
                    <label>Image</label>
                    <?php if (isset($_SESSION['old_image'])) { ?>
                        <div class="post-img" style="background-image: url('/<?= $_SESSION['old_image'] ?>')"></div>
                    <?php } ?>
                    <div>
                        <input type="file" name="image" accept=".gif,.jpg,.png">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-success">
                        <?= (isset($_SESSION['old_id'])) ? 'Edit Post' : 'Add Post' ?>
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
<?php include_once ('components/foooter.php')?>

<script src="/assets/js/post.js" type="text/javascript"></script>

</body>


</html>
