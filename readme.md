**How to install**

Please use virtual host so that the app has an URL for example metro.dev on the localhost (this is important because friendly urls are in action)
Create DB. Open file /app/includes/DbConnect.php and fill in your data (db name, host, user and password)
Create “uploads” folder in root and give it permissions 775 (uploaded images will be stored there)

**DB**

Please import backup.sql file in the root of this repository in order to see some dummy data (users and posts)
For DB connection I have used singleton.

**User**

User has role - user or admin. User with “admin” role may edit/delete/approve posts
User password is made using built-in PHP function password_hash
There are 3 users available after you import sql:
user1@gmail.com
user2@gmail.com
admin@gmail.com

all have same password - 12345

If you try to use email that doesn’t exist or incorrect password you will see the errors.

Once user is logged in he will see the “Welcome, {name}” message top right If user wants to logged out please click on icon next to the name

**Post**

May have either text or image
If user attempts to add both he will see the error.
Once user added the post it has “pending” status and is visible only for admin user and for the owner.

**The structure of the application**

I have tried to separate the application according to MVC patter as much as possible. So views are stored in /pages folder
Controllers are stored in /app/contollers folder. They do the most logic of the application
Models (in our case two - user and post) are stored in app/models folder and mostly do operations with DB

Styles (I used SCSS that was compiled to css using my IDE automatically), js, fonts, images are stored in /assets folder
For all icons on site I have used font icons generated from svg icons using icomoon service

I have used bootstrap framework to make it a little bit nicer.