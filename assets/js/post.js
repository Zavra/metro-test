document.addEventListener("DOMContentLoaded", function(event) {
    //alert(1);

    var contentChange = document.querySelectorAll(".js-post-content");
    contentChange.forEach(function (item, idx) {
        item.addEventListener('click', function () {
            var contentControls = document.querySelectorAll(".post-content-control");
            contentControls.forEach(function (item2, idx) {
                item2.style.display = 'none';
            });

            document.querySelector("#post-" + item.dataset.content).style.display = 'block';

        });
    });

});