<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();
include_once ('includes.php');

$dbInstance = DbConnect::getInstance();
$dbConnection = $dbInstance->getConnection();

$requestURI = array_values(array_filter(explode("/", $_SERVER['REQUEST_URI'])));
$requestMethod = $_SERVER['REQUEST_METHOD'];

$GLOBALS['user'] = checkUser();


if ($requestMethod == 'GET') {

    if (sizeof($requestURI)) {

        switch ($requestURI[0]) {

            case 'login':
                LoginController::index();
                break;

            case 'post':

                if (isset($requestURI[1])) {
                    $postId = $requestURI[1];
                } else { $postId = null; }

                MainController::post($dbConnection, $postId);
                break;

            case 'logout':
                LoginController::logout();
                header("Location: /");
                exit;
                break;

            default:
                echo '404';
                break;

        }
    } else {
        MainController::index($dbConnection);
    }

} else if ($requestMethod == 'POST') {
    switch ($requestURI[0]) {

        case 'login':
            LoginController::login($dbConnection);
            break;

        case 'post':
            MainController::addPost($dbConnection);
            break;

        case 'delete':
            MainController::deletePost($dbConnection);
            break;

        case 'approve':
            MainController::appprovePost($dbConnection);
            break;

        default:
            echo '404';
            break;

    }
}

?>