<?php

function checkUser() {
    if (isset($_SESSION['user'])) {
        $user = unserialize($_SESSION['user']);
    } else {
        $user = null;
    }

    return $user;
}