<?php

class DbConnect {

    private static $instance = null;

    private $connection;

    private $host = 'localhost';
    private $user = 'root';
    private $pass = 'root';
    private $name = 'metro';

    private function __construct()
    {
        $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->name);
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new DbConnect();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

}