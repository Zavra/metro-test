<?php

class LoginController
{
    public static function index()
    {
        $title = 'Metro Login';
        include_once ("pages/login.php");

        unset($_SESSION['old_email']);
        unset($_SESSION['old_password']);
        unset($_SESSION['form_error']);

    }

    public static function login($dbConnection)
    {
        if ( isset($_POST['email']) && isset($_POST['password']) ) {
            $user = new User($dbConnection);
            $result = $user->login($_POST['email'], $_POST['password']);

            if ($result['error']) {
                $_SESSION['old_email'] = $_POST['email'];
                $_SESSION['old_password'] = $_POST['password'];
                $_SESSION['form_error'] = $result['msg'];
                header("Location: /login");
                exit;
            } else {
//                var_dump($user); exit;
                $_SESSION['user'] = serialize($user);
                header("Location: /");
                exit;
            }
        }
    }

    public static function logout()
    {
        unset($_SESSION['user']);
    }
}
