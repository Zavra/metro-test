<?php

class MainController
{

    /** show home page with a list of posts */
    public static function index($dbConnection)
    {
        $title = 'Metro Guestbook';

        /** order posts by date, show newest first */
        $sqlOrderBy = " ORDER BY created_date DESC";

        if (!empty($GLOBALS['user']) && $GLOBALS['user']->isAdmin()) {
            /** select all posts if user is admin */
            $stmt = $dbConnection->prepare("SELECT * FROM posts" . $sqlOrderBy);
        } else if (!empty($GLOBALS['user'])) {
            /** for logged in users select all active posts and pending posts of his own */
            $userId = $GLOBALS['user']->getId();
            $stmt = $dbConnection->prepare("SELECT * FROM posts WHERE status = 'active' OR user_id = ?" . $sqlOrderBy);
            $stmt->bind_param('s', $userId);
        } else {
            /** for all others show only active posts */
            $stmt = $dbConnection->prepare("SELECT * FROM posts WHERE status = 'active'" . $sqlOrderBy);
        }

        $stmt->execute();

        $result = $stmt->get_result();

        if ($result) {
            $postsDb = $result->fetch_all(MYSQLI_ASSOC);
            $posts = [];
            foreach($postsDb as $post) {
                $postModel = new Post($dbConnection, $post);
                $posts[] = $postModel;
            }
        } else {
            $posts = null;
        }

        include_once ("pages/home.php");

        /** show messages only once, aka flash session */
        unset($_SESSION['msg_success']);
        unset($_SESSION['msg_error']);

    }

    /** show page for add/edit post */
    public static function post($dbConnection, $postId) {

        /** user should be logged in in order to add the post */
        if (!empty($GLOBALS['user'])) {

            if (!empty($postId)) {

                $title = 'Metro Edit Post';
                $postModel = new Post($dbConnection);
                $postModel->find($postId);

                /** show edit page for admin user only */
                if ($GLOBALS['user']->isAdmin()) {

                    $_SESSION['old_id'] = $postModel->id;
                    $_SESSION['old_title'] = $postModel->title;

                    if(!empty($postModel->text)) {
                        $_SESSION['old_text'] = $postModel->text;
                    }
                    if(!empty($postModel->image)) {
                        $_SESSION['old_image'] = $postModel->image;
                    }

                } else {
                    $_SESSION['msg_error'] = 'You are not allowed to edit post!';
                    header("Location: /");
                    exit;
                }

            } else {
                $title = 'Metro New Post';
            }

            include_once ("pages/post.php");

            unset($_SESSION['old_id']);
            unset($_SESSION['old_title']);
            unset($_SESSION['old_text']);
            unset($_SESSION['old_image']);
            unset($_SESSION['msg_error']);


        } else {
            $_SESSION['msg_error'] = 'Please login to add post!';
            header("Location: /");
            exit;
        }
    }

    /** fired when add/edit post form is submitted */
    public static function addPost($dbConnection) {

        if (!empty($GLOBALS['user'])) {

            $postText = $_POST['text'];
            $postImageName = $_FILES["image"]["tmp_name"];

            if ( !empty($_POST['title']) && (!empty($postText) || !empty($postImageName))) {

                if (!empty(trim($postText)) && !empty($postImageName)) {
                    self::redirectWithMsg('You are not allowed to add both text and image!');
                }

                if (!empty($postImageName)) {
                    $checkImage = getimagesize($postImageName);
                    if($checkImage === false) {
                        self::redirectWithMsg('Uploaded image seems to be corrupted! Please try again.');
                    }

                    if ($_FILES["image"]["size"] > 2097152) {
                        self::redirectWithMsg('Uploaded image is too big! Allowed size is 2Mb');
                    }
                }

                $postModel = new Post($dbConnection);

                if (isset($_POST['id'])) {
                    $result = $postModel->edit();
                    if ($result) {
                        $_SESSION['msg_success'] = 'Post was successfully edited!';
                    } else {
                        $_SESSION['msg_error'] = 'We are sorry, but something went wrong. Try again!';
                    }
                } else {
                    $result = $postModel->create();
                    if ($result) {
                        $_SESSION['msg_success'] = 'Post was successfully added!';
                    } else {
                        $_SESSION['msg_error'] = 'We are sorry, but something went wrong. Try again!';
                    }
                }
            } else {
                self::redirectWithMsg('Please choose text or image.');
            }
        } else {
            $_SESSION['msg_error'] = 'Please login to add post!';
        }

        header("Location: /");
        exit;
    }

    private function redirectWithMsg($error) {

        $_SESSION['msg_error'] = $error;
        if (isset($_POST['id'])) {
            $_SESSION['old_id'] = $_POST['id'];
        }

        $_SESSION['old_title'] = $_POST['title'];

        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit;

    }

    /** fired when delete post form is submitted  (delete button is clicked) */
    public static function deletePost($dbConnection) {

        /** only admin can do this */
        if (!empty($GLOBALS['user']) && $GLOBALS['user']->isAdmin()) {

            if (isset($_POST['post_id'])) {
                $postModel = new Post($dbConnection);
                $result = $postModel->delete($_POST['post_id']);
                if ($result) {
                    $_SESSION['msg_success'] = 'Post was successfully deleted!';
                } else {
                    $_SESSION['msg_error'] = 'We are sorry, but something went wrong. Try again!';
                }
            }

        } else {
            $_SESSION['msg_error'] = 'You are not allowed to perform this action!';
        }

        header("Location: /");
        exit;
    }

    /** fired when approve post form is submitted  (approve button is clicked) */
    public static function appprovePost($dbConnection) {

        if (!empty($GLOBALS['user']) && $GLOBALS['user']->isAdmin()) {

            if (isset($_POST['post_id'])) {
                $postModel = new Post($dbConnection);
                $result = $postModel->approve($_POST['post_id']);
                if ($result) {
                    $_SESSION['msg_success'] = 'Post was successfully approved and now visible for all!';
                } else {
                    $_SESSION['msg_error'] = 'We are sorry, but something went wrong. Try again!';
                }
            }

        } else {
            $_SESSION['msg_error'] = 'You are not allowed to perform this action!';
        }

        header("Location: /");
        exit;
    }

}