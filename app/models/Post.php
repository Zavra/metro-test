<?php

class Post {

    private $connection;

    public $id;
    public $title;
    public $text;
    public $image;
    public $user;
    public $status;
    public $date;

    public function __construct($dbConnection, $post = null)
    {
        $this->connection = $dbConnection;

        $this->id = $post['id'];
        $this->title = $post['title'];

        if (isset($post['text'])) {
            $this->text = $post['text'];
        }

        if (isset($post['image'])) {
            $this->image = $post['image'];
        }

        $this->status = $post['status'];
        $this->date = strtotime($post['created_date']);

        $user = new User($this->connection);
        $user->find($post['user_id']);
        $this->user = $user;

    }

    public function find($id)
    {
        $stmt = $this->connection->prepare("SELECT * FROM posts WHERE id = ?");
        $stmt->bind_param('s', $id);
        $stmt->execute();

        $result = $stmt->get_result();
        $postDb = $result->fetch_object();

        if (!empty($postDb)) {
            $this->id = $postDb->id;
            $this->title = $postDb->title;
            $this->text = $postDb->text;
            $this->image = $postDb->image;
            $this->status = $postDb->status;
            $this->date = strtotime($postDb->created_date);

            $user = new User($this->connection);
            $user->find($postDb->user_id);
            $this->user = $user;

        }
    }

    public function create() {

        $userId = $GLOBALS['user']->getId();
        $this->title = $_POST['title'];
        $this->textOrImage();

        try {
            $stmt = $this->connection->prepare("INSERT INTO posts (user_id, title, text, image) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssss", $userId, $this->title, $this->text, $this->image);
            $stmt->execute();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function edit() {

        $this->id = $_POST['id'];
        $this->title = $_POST['title'];
        $this->textOrImage();

        try {
            $stmt = $this->connection->prepare("UPDATE posts SET title=?, text=?, image=? WHERE id=?");
            $stmt->bind_param("ssss", $this->title, $this->text, $this->image, $this->id);
            $stmt->execute();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function approve($id) {
        try {
            $stmt = $this->connection->prepare("UPDATE posts SET status='active' WHERE id=?");
            $stmt->bind_param("s", $id);
            $stmt->execute();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            $stmt = $this->connection->prepare("DELETE FROM posts WHERE id=?");
            $stmt->bind_param("s", $id);
            $stmt->execute();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    private function textOrImage() {
        if (!empty($_FILES["image"]["tmp_name"])) {
            $this->text = null;
            $imageType = strtolower(pathinfo(basename($_FILES["image"]["name"]),PATHINFO_EXTENSION));
            $imagePath = "uploads/" . md5(rand()) . '.' . $imageType;

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath)) {
                $this->image = $imagePath;
            } else {
                return false;
            }

        } else {
            $this->text = $_POST['text'];
            $this->image = null;
        }
    }

}