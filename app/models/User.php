<?php

class User {


    private $connection;
    private $id;
    private $email;
    private $role;
    private $name;

    public function __construct($dbConnection)
    {
        $this->connection = $dbConnection;
    }

    public function login($email, $password)
    {
        $stmt = $this->connection->prepare("SELECT * FROM users WHERE email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();

        $result = $stmt->get_result();
        $userDb = $result->fetch_object();

        if (!empty($userDb)) {
            if ( password_verify( $password, $userDb->password ) ) {
                $this->fillModel($userDb);

                return ['error' => false];
            } else {
                return ['error' => true, 'msg' => 'Password is incorrect'];
            }
        } else {
            return ['error' => true, 'msg' => 'No user found with such email'];
        }

    }

    public function find($id)
    {
        $stmt = $this->connection->prepare("SELECT * FROM users WHERE id = ?");
        $stmt->bind_param('s', $id);
        $stmt->execute();

        $result = $stmt->get_result();
        $userDb = $result->fetch_object();

        if (!empty($userDb)) {
            $this->fillModel($userDb);
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function isAdmin() {
        return ($this->role == 'admin') ? true : false;
    }

    private function fillModel($userDb) {
        $this->id = $userDb->id;
        $this->email = $userDb->email;
        $this->name = $userDb->name;
        $this->role = $userDb->role;
    }

}