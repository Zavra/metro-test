<?php

    include_once('app/includes/DbConnect.php');
    include_once('app/includes/Helpers.php');

    include_once('app/models/User.php');
    include_once('app/models/Post.php');

    include_once ('app/controllers/MainController.php');
    include_once ('app/controllers/LoginController.php');

?>